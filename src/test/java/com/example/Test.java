package com.example;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieRuntimeFactory;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNModel;
import org.kie.dmn.api.core.DMNResult;
import org.kie.dmn.api.core.DMNRuntime;

public class Test {
    public static void main(String[] args) {
        KieServices ks = KieServices.Factory.get();
        KieContainer kcontainer = ks.getKieClasspathContainer();
        DMNRuntime dmnRuntime = KieRuntimeFactory.of(kcontainer.getKieBase())
                                                 .get(DMNRuntime.class);
        DMNModel dmnModel = dmnRuntime.getModels()
                                      .get(0);
        DMNContext context = dmnRuntime.newContext();

        context.set("PreviousIncidents", false);
        context.set("Age", 21);
        context.set("CarAge", 3);

        DMNResult result = dmnRuntime.evaluateAll(dmnModel, context);

        System.out.format("Final Price: %.2f, Base Price: %.2f\n", 
                            result.getContext().get("FinalPrice"),
                            result.getContext().get("Insurance Base Price"));
    }
}