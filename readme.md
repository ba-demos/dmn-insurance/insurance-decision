# DMN Insurance demo

## DMN

![dmn](doc/dmn-insurance-price-04.png)

![dmn](doc/dmn-insurance-price-01.png)

![dmn](doc/dmn-insurance-price-02.png)

![dmn](doc/dmn-insurance-price-03.png)

## REST API

Container name: `InsuranceDecision_1.0-SNAPSHOT`

POST request to `http://localhost:8080/kie-server/services/rest/server/containers/InsuranceDecision_1.0-SNAPSHOT/dmn`

with payload:

    {
        "model-namespace" : "http://www.trisotech.com/definitions/_bb8b9304-b29f-462e-9f88-03d0d868aec5",
        "model-name" : "Insurance Pricing",
        "decision-name" : [ ],
        "decision-id" : [ ],
        "decision-service-name" : null,
        "dmn-context" : {"PreviousIncidents" : false,"CarAge" : 3,"Age" : 28}
    }

Expected result:

    {
        "type" : "SUCCESS",
        "msg" : "OK from container 'InsuranceDecision_1.0-SNAPSHOT'",
        "result" : {
            "dmn-evaluation-result" : {
                "messages" : [ ],
                "model-namespace" : "http://www.trisotech.com/definitions/_bb8b9304-b29f-462e-9f88-03d0d868aec5",
                "model-name" : "Insurance Pricing",
                "decision-name" : [ ],
                "dmn-context" : {
                    "PreviousIncidents" : false,
                    "Insurance Base Price" : 1000,
                    "Car reliability factor" : "function Car reliability factor( car age )",
                    "CarAge" : 3,
                    "Age" : 28,
                    "FinalPrice" : 1100.0
                },
                "decision-results" : {
                    "_7c68efef-3b20-4807-8d15-7f55995cc8fd" : {
                    "messages" : [ ],
                    "decision-id" : "_7c68efef-3b20-4807-8d15-7f55995cc8fd",
                    "decision-name" : "Insurance Base Price",
                    "result" : 1000,
                    "status" : "SUCCEEDED"
                    },
                    "_75adccac-8a37-4312-94b7-48ec3236afde" : {
                    "messages" : [ ],
                    "decision-id" : "_75adccac-8a37-4312-94b7-48ec3236afde",
                    "decision-name" : "FinalPrice",
                    "result" : 1100.0,
                    "status" : "SUCCEEDED"
                    }
                }
            }
        }
    }
